from request_handler import handle_request
from response_handler import handle_response


def lambda_handler(event, context):

    return handle_response(handle_request(event))
