import zipfile
import json
import boto3

s3 = boto3.resource('s3')
lmda = boto3.client('lambda')


def handle_request(request):
    bucket_name = request['bucket-info']['bucket']
    layer_name = request['layer-info']['layer-name']

    build_zip_file(bucket_name)
    return publish_layer(layer_name, zipfile_as_bytes())


def build_zip_file(bucket_name):
    layer_zip = zipfile.ZipFile('/tmp/model-layer.zip', 'w')
    model_bucket = s3.Bucket(bucket_name)
    meta_data_info = {}
    for obj in model_bucket.objects.filter(Prefix='models/', Delimiter='/'):
        # Still need to check for _MODEL otherwise hidden objects get pulled in
        if obj.key.endswith('_MODEL'):
            # key name still has the forward slash (models/), so split it off before writing to zip file
            file_name = obj.key.split('/')[1]
            model_bucket.download_file(obj.key, '/tmp/' + file_name)
            layer_zip.write('/tmp/' + file_name, 'python/' + file_name, zipfile.ZIP_DEFLATED)

            # find the name for the key from models/KEYNAME_MODEL
            last_underscore = obj.key.rfind('_')
            slash = obj.key.find('/') + 1
            # if filename is mymodel, will add as key=mymodel value=mymodel_MODEL
            meta_data_info[obj.key[slash:last_underscore]] = file_name

    # add metadata in the form of name:model_file
    layer_zip.writestr('python/metadata', json.dumps(meta_data_info), zipfile.ZIP_DEFLATED)

    layer_zip.close()


def publish_layer(layer_name, byte_content):
    to_return = {'has_error': False}
    try:
        resp = lmda.publish_layer_version(
            LayerName=layer_name,
            Description='Layer containing markovify model files',
            Content={
                'ZipFile': byte_content
            },
            CompatibleRuntimes=['python3.6']
        )
        to_return['LayerArn'] = resp['LayerArn']
        to_return['Version'] = resp['Version']

    except (lmda.exceptions.ServiceException, lmda.exceptions.TooManyRequestsException, lmda.exceptions.CodeStorageExceededException) as e:
        to_return['has_error'] = True
        to_return['error'] = e['Error']['Message']

    return to_return


def zipfile_as_bytes():
    byte_content = bytes(1)
    with open('/tmp/model-layer.zip', 'rb') as file_data:
        byte_content = file_data.read()

    return byte_content
