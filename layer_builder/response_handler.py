import json


def handle_response(validation):
    message = "Published new layer version"
    details = {}
    status_code = 201

    if validation['has_error'] is True:
        message = 'Could not publish new layer version'
        details['error'] = validation['error']
        status_code = 500
    else:
        details['layer-arn'] = validation['LayerArn']
        details['version'] = validation['Version']

    return {
        "statusCode": status_code,
        "body": json.dumps({
            "message": message,
            "details": details
        }),
    }
